
# Goxplorer, a Bitcoin blockchain parser in _golang_

`Goxplorer` began as a toy project I've been working on to better understand blockchain's internal. It has evolved into a rather complete Bitcoin blockchain explorer and an easy to use block parser package for _Go_.

`Goxplorer` can also act as an _HTTP REST JSON_  server, in this mode it permits everything available on the command line, but with _HTTP_ requests.

A couple of similar project exist, yet none of them seem complete nor very well documented and easy to use, so I took the opportunity and here it is. I think.

## Download

Either clone this `git` repository and follow _build_ instructions below

```sh
$ git clone git@gitlab.com:iMil/goxplorer.git
```

Or go to the [releases](https://gitlab.com/iMil/goxplorer/-/releases) page where you will find binary releases for most common platforms.

## Status

`Goxplorer` has locking / pubkey support for `P2PK`, `P2PKH`, `P2SH`, `P2MS` and full [SegWit](https://en.wikipedia.org/wiki/SegWit) support for native `P2WPKH` and `P2WSH` along with `P2SH-P2WPKH` and `P2SH-P2WSH`. Unlocking / _ScriptSig_ support for all of the above is also complete.  
While these features are tested using the _Go testing_ infrastructure, there might be errors hidden in some special cases like non-standard _P2SH_ inputs.

A nice feature of this project is the ability to create callback functions that are executed for each transaction and / or block. Also, all the useful fields are tagged as `JSON` and thus can be easily `Marshal`'ed and used.

An enormous thank you to [Greg Walker](https://learnmeabitcoin.com/about) for his
exceptional work on [Learn Me A Bitcoin](https://learnmeabitcoin.com), and to
[cvasqxz](https://github.com/cvasqxz/) for the initial inspiration.

## Build

`$ make`

## Usage

```code
Usage of ./goxplorer:
  -a    load addresses (default: false)
  -ac string
        all blocks callback
  -acparam string
        all blocks callback optional parameter
  -addr string
        address: search an address in the chainstate database (slow!)
        mkdb: create an address database at $BTCADDRDB
        db:<address> search address UTXOs at $BTCADDRDB
        txid:<txid> search <txid> addresses, height and amounts
  -b uint
        block file number (i.e. 1845)
  -bc string
        block callback
  -bcparam string
        block callback optional parameter
  -d string
        load block file matching with a date of format 2019-12-24[@02:23:10]
  -e int
        start reading at block's height, autoloads blk file
  -f string
        block file path
  -i    guess and load inputs addresses (default: false)
  -j    print loaded blocks in JSON (default: false)
  -l string
        start reading at block's hash, autoloads blk file
  -ldb string
        print LevelDB <key> infos for given parameter and exit
  -lscb
        list callbacks
  -n int
        number of blocks to load (default: all)
  -r    load serialized raw ins and outs (default: false)
  -s int
        start at block #
  -t    compute and load TXIDs (default: false)
  -tc string
        transaction callback
  -tcparam string
        transaction callback optional parameter
  -tx string
        raw hex serialized transaction
  -w    start HTTP REST server
  -x    compute and load block hashes (default: false)
```

### Command line demo

Before using `goxplorer` either on the command line or as an HTTP server, you may want to set the `BTCBLOCKSHOME` variable. If you do not, it will default to `${HOME}/.bitcon/blocks`, which is `bitcoind`'s default.

```sh
$ export BTCBLOCKSHOME=${HOME}/copy/of/blocks
```
This is where the program will look for actual block files and by default, the _LevelDB_ database in `${BTCBLOCKSHOME}/index`, which can be overridden with the `BTCBLOCKINDEX` variable.  

⚠ Please **don't** use `goxplorer` against your real block index, `LevelDB` does not support parallel queries, neither read or write ⚠  
Instead, make a copy of it and set a `BTCBLOCKINDEX` variable pointing to this copy **or** use an [overlay filesystem](https://www.grant.pizza/blog/overlayfs/).

In order to perform **address or TXID based search**, it is necessary to build an address database using _Bitcoin_'s blockchain `chainstate` _LevelDB_ database. Again, you should use a copy of it, and not the original `chainstate` database, or use an _Overlay FileSystem_. By default, this `BTCCHAINSTATE` variable points to `${BTCBLOCKSHOME}/chainstate`.

This new database uses [Badger](https://github.com/dgraph-io/badger) because it is more suited for concurrent accesses, which is convenient if you plan on using `goxplorer` in HTTP/REST mode  
For further speed and usability, `goxplorer` also dumps the block index database in the address _Badger_ database.

At the time of writing this (May 2020), the address database size is about 20G.

Building the address database is pretty simple:

```sh
$ export BTCADDRDB=myownaddrdb
$ ./goxplorer -addr mkdb
```
This will create an address database in the `myownaddrdb` directory (by default it is set to `addrdb`), located in the current working directory.  
Depending on your hardware, this operation, as of May 17th 2020, takes approximatively 1 hour.  
Note that this database does not contain **all** used _Bitcoin_ addresses bu only those with unspent amount in it ([UTXO](https://bitcoin.org/en/glossary/unspent-transaction-output)).

#### Examples

Load 2 blocks from `blk00030.dat`, including `txid`s and `addresses`, then print the output in _JSON_

```sh
$ ./goxplorer -b 30 -n 2 -t -a -j
```

Count the number of _SegWit_ transactions in block 3 (read 1 block `-n 1`, starting at block 3 `-s 3`).

```sh
$ ./goxplorer -b 1870 -s 3 -n 1 -j|jq -r '[.[].transacdata[]|select(.segwit == true)]|length'
1990
```

Find out which is the `txid` for a transaction containing a particular _output address_:

```sh
$ ./goxplorer -b 1870 -s 3 -n 1 -j -t -a|jq -r '.[].transacdata[]|{txid: .txid, output: .outputs[]|select(.address == "1NDyJtNTjmwk5xPNhjgAMu4HDHigtobu1s")}'
{
  "txid": "ce079e8659c355068a0b638402c11290c95ff64bcc9a6bd8dc0fb14b1caafe2d",
  "output": {
    "value": 10000000000,
    "spubkeysize": 25,
    "spubkey": "dqkU6M6jCYm9FVMPgZt2ZoSwDce6fPqIrA==",
    "address": "1NDyJtNTjmwk5xPNhjgAMu4HDHigtobu1s",
    "locktype": "P2PKH"
  }
}
...
```

Output a raw serialized transaction for a given `txid`:

```sh
$ ./goxplorer -b 1845 -s 3 -n 1 -j -r -t|jq -r '.[].transacdata[]|select(.txid == "3c545cfe89ec6614d083728211bca2d66a215027efdc4597d9970f473e7a4989")|.raw'|base64 -d|xxd -p|tr -d '\n'
01000000000101f6901ba064d6d5a34815596b7f3114a937dd36c23fed9f275c2df197c566a22e0100000000ffffffff0240981e090000000017a914f3df5032df6c05aa38b8899e3186132052d3e461870e073b0500000000220020701a8d401c84fb13e6baf169d59684e17abd9fa216c8cc5b9fc63d622ff8c58d040047304402204da66f456ab24ebc2e4472aed14d70f0ce73b149129c1e1129437d978878076f02200d3ce14c78939e06f771ec1ec93dcda5a318a1e7c25f8bee5315db31f2c4b7cf014730440220752bd175310c47969324d903a984f672069e74153010bd274f357793126686a002206315f0bf7490e0bf4b5dad2dff05a3f7420a3293ccf45b5efc2bcd054fae1c68016952210375e00eb72e29da82b89367947f29ef34afb75e8654f6ea368e0acdfd92976b7c2103a1b26313f430c4b15bb1fdce663207659d8cac749a0e53d70eff01874496feff2103c96d495bfdd5ba4145e3e046fee45e84a8a48ad05bd8dbb395c011a32cf9f88053ae00000000
```

Load a block using its hash as reference, this will lookup in [LevelDB](https://en.wikipedia.org/wiki/LevelDB) Block Index Record directory pointed by the `BTCBLOCKINDEX` environment variable and find the corresponding `blk.dat` file and position in file. `BTCBLOCKINDEX` defaults to `${BTCBLOCKSHOME}/index`. `BTCBLOCKSHOME` points to the location where block files are located, by default in `${HOME}/.bitcoin/blocks`.

```sh
$ export BTCBLOCKINDEX=./index
$ ./goxplorer -l 000000000000000000147a7c7d848682df32f5c65e321f444ef7f18904f9a1f2 -n 1 -x -t -j|jq -r '.[]|"txid: " + .transacdata[].txid, "blockhash: " + .blockhash'
txid: c172e25752ab7a9e5e96eacde8a1087d75d78aaf7a7f0bd4d0f3b33d7d958bea
txid: f624d3ed13dee54426fb58165c2ef51d8574223bcb0325fe3248dd4e08aa2d8f
txid: 2dac7806b3067364cd889d7be8f513b0ab40e36d6979f53f83631a911f85ff65
txid: 57cd2efb0f597c54c0669dfd3ee3265b160704793f55496e41a1a0152b386655
txid: 992f6eeeb4446d429f6480e0d217407ca410a35ebc92bfaf448886f67d7033be
txid: 7b730664d85eed7e5a7785cb8fdd023be2d78a3ca1c315852f2bf891aee8d88d
txid: 9d0a8fbd7597088ed53412d48edabc431468919ebe858e4048832bf0105460d0
txid: 18408e8351499f8d415885092113cada7661152cf11a77123b46764b1c0fb9e2
txid: bed9aca7a3991e5c2b3a0baae2bd486c8c8aa03efde223bb7bc3ef24abcae5b3
txid: adbcf5f5fa0568637c02fe6b1edbc497ab76971c20e19cc63570eff823b30dae
txid: 7de2f44b5278f35a8bd554252702b7baf0d5fa2ef52b1a28767487dfb96ada01
blockhash: 0000000000000000081c69c27e9caac66ac6c5f420c799532521fd7b7b05ddfc
```

Load a block using its height and print the first 10 output addresses. Here again, `BTCBLOCKINDEX` and / or `BTCBLOCKSHOME` must be set.

```sh
$ echo $BTCBLOCKSHOME # goxplorer will search ./index as the LevelDB path
.
$ ./goxplorer -e 601629 -j -n 1 -a|jq -r '.[].transacdata[].outputs[].address'|head -10
1KFHE7w8BhaENAswwryaoccDb6qcT6DbYY
1CcX4NNf4AKTR1KTRrZx8xMVfJjtrxz7MB
1AuovnUxXN4an1UEbSb1W26d1SJiwDfb6R
3FT5QmfyFZpDK9Me8UdD4N6hFL5MUi6iSF
3HdfFaF4Hcj6ujBA6GsREvw1C7SaVYDHww
14zV5ZCqYmgyCzoVEhRVsP7SpUDVsCBz5g
1Po1oWkD2LmodfkBYiAktwh76vkF93LKnh
```
Load a block file corresponding to a date, again, `BTCBLOCKINDEX` and / or `BTCBLOCKSHOME` must be set, here we get the height of the first block mined on November 11, 2019:

```sh
$ ./goxplorer -d 2019-11-21 -j -n 1|jq -r '.[].transacdata[0].inputs[0].height'
604680
```

`Goxplorer` can be use to inspect block index values (in the _LevelDB_ database) for a given _hash_ or _file_:

```sh
$ ./goxplorer -l 000000000000000000147a7c7d848682df32f5c65e321f444ef7f18904f9a1f2 -ldb block
{
  "nversion": 180100,
  "nheight": 601629,
  "nstatus": 157,
  "ntx": 2456,
  "nfile": 1845,
  "ndatapos": 8,
  "nundopos": 4186963
}
$ ./goxplorer -b 1845 -ldb file
{
  "nblocks": 127,
  "nsize": 133112994,
  "nundosize": 18090258,
  "nheightfirst": 600753,
  "nheightlast": 601727,
  "ntimefirst": 1571860166,
  "ntimeLast": 1572487584
}
```

Here's an example of using the (very naive) `csv` callback to return desired fields in `CSV` format:

```sh
$ ./goxplorer -b 1870 -tc csv -tcparam lsfields
version
incount
inputs.prevtxid
inputs.prevout
inputs.ssigsize
inputs.ssig
...
$ ./goxplorer -t -b 1845 -n 2 -a -tc csv -tcparam outputs.address,txid
1KFHE7w8BhaENAswwryaoccDb6qcT6DbYY null null null, df882c43a0441b2f9b14ffd39738a6a36cda3b8c11279c7d0f93ccd5002c9482
1KFHE7w8BhaENAswwryaoccDb6qcT6DbYY null null null, df882c43a0441b2f9b14ffd39738a6a36cda3b8c11279c7d0f93ccd5002c9482
1CcX4NNf4AKTR1KTRrZx8xMVfJjtrxz7MB 1AuovnUxXN4an1UEbSb1W26d1SJiwDfb6R, 1bdb3a4623e76c002478b551ff7fa2f5179c8afc49ef9e7903590de5211ffca9
3FT5QmfyFZpDK9Me8UdD4N6hFL5MUi6iSF 3HdfFaF4Hcj6ujBA6GsREvw1C7SaVYDHww 14zV5ZCqYmgyCzoVEhRVsP7SpUDVsCBz5g, d57d044efc2181e61b72422fec01e24344d5f6e1eb095bef571bc93d1dfba6a8
1Po1oWkD2LmodfkBYiAktwh76vkF93LKnh null 16WttDJL4eJS9qcDCndRyhbdTAUYihYyfN, b6bbc0db9436eb57ad202dce9a0cd4c5e1dac169f9d342eea65a40109f000eb1
...
```

Search for an address _UTXO_'s using the previously created address database.

```sh
$ ./goxplorer -addr db:addrdb:199XtF5eyyG1tFgJknwcUhmkDo2pGjx3Pq|jq '.[0]'
{
  "txid": "00329e080dde6a4d7047b40f1c142a210de1bcadcd99b8d257c8d7da8c54c9a4",
  "height": 51478,
  "nsize": 0,
  "address": "199XtF5eyyG1tFgJknwcUhmkDo2pGjx3Pq",
  "amount": 500000
}
```

Note that you can search either a full or a partial address:

```sh
$ ./goxplorer -addr db:addrdb:1BVxyPYXkV5tF|jq '.[0]'
{
  "txid": "b5ec0c758e34ff541d20cf7f43b43a748293b7e8262b845f72708323a96f1e00",
  "height": 492634,
  "nsize": 0,
  "address": "1BVxyPYXkV5tFKWMnVoeQgUZEzkrAsDUVB",
  "amount": 600
}
```

In the same manner, you can search for a `TXID`:

```sh
$ ./goxplorer -addr txid:3f4ef2469c6dcb8f3255ca444d237a73c8df7f20cf6d5c3b17f59c34934d1b00|jq '.[0]'
{
  "txid": "3f4ef2469c6dcb8f3255ca444d237a73c8df7f20cf6d5c3b17f59c34934d1b00",
  "height": 425174,
  "nsize": 0,
  "address": "1FhpzVpBdmN6iLdEnzffFhBo1dgXJ5W9Ec",
  "amount": 300
}
```

For information and research purposes, it is also possible to simply print **all** _UTXO_ informations to the console:

```sh
$ ./goxplorer -addr print
{
  "txid": "cbf06ed1d9398130cc57ef620f670919cf4323b317a6bba6c4aeb88179010000",
  "height": 629564,
  "nsize": 1,
  "address": "349T278NnFcyCnHfvmjvsH1daVfVYE7wDL",
  "amount": 329169
}
...
```

### HTTP REST/JSON query

First, start `goxplorer` in _HTTP server_ mode. It listens on port **7554** (_BTC_ price when I stared working on this feature ;) )
If the server must read blocks in another path than the standard `${HOME}/.bitcoin/blocks`, declare and export the `BTCBLOCKSHOME` variable.  
Also be sure to set `BTCADDRDB` pointing to the address database if you are to search using addresses and named it otherwise than `addrdb`.

```sh
$ export BTCBLOCKSHOME=$(pwd)
$ export BTCADDRDB=myaddressdb
$ ./goxplorer -w
```

Query the server for a single _block_ from _2019 October 10th_ at _3:15:00 PM_, and load output addresses:

```sh
$ curl -H "Content-Type: application/json" -XPOST --data '{"nblocks": 1, "loadaddr": true}' http://localhost:7554/block/date/2019-10-30@15:15:00|jq -r '.[].transacdata[0].outputs[0].address'
1KFHE7w8BhaENAswwryaoccDb6qcT6DbYY
```

Retrieve previous _TXID_ for the first input from a single raw transaction:

```sh
tx=010000000001010000000000000000000000000000000000000000000000000000000000000000ffffffff64031d2e092cfabe6d6dfd4ad76684d950f0d552c65b65f4327f8cdc59dabf5667188a7a5af5945d29cf10000000f09f909f00104d696e6564206279207768787a383838000000000000000000000000000000000000000000000000000000050060290000000000000462c9044c000000001976a914c825a1ecf2a6830c4401620c3a16f1995057c2ab88ac00000000000000002f6a24aa21a9ed5234157ea6c81de0dbf0ced1969164328c4992e2f895deadbb1da2d75af1d23b08000000000000000000000000000000002c6a4c2952534b424c4f434b3a4c26568b3ec880eff3c406ace7c18d27cdd710de2c4e5f7c590fb22e001bf1730000000000000000266a24b9e11b6df3af861a7b874ec0565192fd6cc88e56fb1310d2ad37ca999da9112c0b8b72b101200000000000000000000000000000000000000000000000000000000000000000bd1c4c3a
$ curl -s -H "Content-Type: application/json" -XPOST --data "{\"loadaddr\": true, \"tx\": \"$tx\"}" http://localhost:7554/tx/|jq -r '.[].transacdata[0].inputs[0].prevtxid'
0000000000000000000000000000000000000000000000000000000000000000
```

There are as many query types as in the command line:

* `/block/file/<file>`: load this block file number (`blk` and `.dat` are automatically prepended and appended)
* `/block/date/<date>`: load block file matching with a date of format `2006-01-02[@15:04:05]`
* `/block/height/<height>`: start reading at block's height, autoloads blk file
* `/block/hash/<hash>`: start reading at block's hash, autoloads blk file
* `/tx/`: process raw hex serialized transaction
* `/leveldb/block/<hash>`: print LevelDB block values corresponding to `hash`
* `/leveldb/file/<blockfile number>`: print LevelDB file values corresponding to `blockfile number`
* `/utxo/<address>`: retrieves TXID, height and amount list for an address
* `/utxo/<txid>`: retrieves address, height and amount list for an UTXO

Load parameters can be passed to the query as as _JSON_ data structure with the following parameters:

* `nblocks: <int>`: number of blocks to load (default: all)
* `startblock: <int>:` start at block #
* `loadaddr: <bool>`: load addresses (default: false)
* `loadinaddr: <int>`: guess and load inputs addresses (default: false) 
* `loadtxid: <bool>`: compute and load _TXIDs_ (default: false)
* `loadblockhash: <bool>`: compute and load block hashes (default: false)
* `loadraw: <bool>`: load serialized raw ins and outs (default: false)
* `tx: <string>`: raw transaction

### Using the `blockchain` package

Consider the following program:

```go
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"

	"gitlab.com/iMil/goxplorer/blockchain"
)

func main() {
	blockfile := flag.String("b", "", "full path for block file")
	a := flag.Bool("a", false, "load addresses (default: false)")
	i := flag.Bool("i", false, "guess and load inputs addresses (default: false)")
	j := flag.Bool("j", false, "print loaded blocks in JSON (default: false)")
	n := flag.Int("n", 0, "number of blocks to load (default: all)")
	r := flag.Bool("r", false, "load serialized ins and outs (default: false)")
	s := flag.Int("s", 0, "start at block # (default: 0)")
	t := flag.Bool("t", false, "compute and load TXIDs (default: false)")
	x := flag.Bool("x", false, "compute and load block hashes (default: false)")
	bc := flag.String("bc", "", "block callback (default: none)")
	tc := flag.String("tc", "", "transaction callback (default: none)")

	flag.Parse()

	if len(*blockfile) == 0 {
		log.Fatal("no block file given")
	}

	// loading parameters, here we want to compute and load TXIDs, and load
	// inputs and outputs addresses. We also pass a helper function to the
	// transaction loading so we see something happening while reading the
	// block file.
	//
	// only load the n blocks from the block file
	blockchain.Params.NBlocks = uint32(*n)
	// compute and load TXIDs
	blockchain.Params.LoadTxid = *t
	// compute and load block hashes
	blockchain.Params.LoadBlockHash = *x
	// start loading at block s
	blockchain.Params.StartBlock = uint32(*s)
	// load output addresses
	blockchain.Params.LoadAddr = *a
	// guess and load input addresses
	blockchain.Params.LoadInAddr = *i
	// load in and out raw data
	blockchain.Params.LoadRaw = *r
	// execute the function mapped to this key for each block
	blockchain.Params.BlockFunc = callbacks.Callbacks[*bc]
	// parameters for the block callback
	blockchain.Params.BFuncParam = *bcparam
	// execute the function mapped to this key for each transaction
	blockchain.Params.TransacFunc = callbacks.Callbacks[*tc]
	// parameters for the transaction callback
	blockchain.Params.TFuncParam = *tcparam

	// load blockfile with previously declared parameters
	b, err := blockchain.LoadBlocks(*blockfile)
	if err != nil {
		log.Fatal(err)
	}

	// print loaded locks as JSON formatted data
	if *j {
		js, err := json.MarshalIndent(b, "", "  ")
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(string(js))
	}
}
```

When called like in the _Demo_ section, it will simply read the `blk00030.dat` file and print out a _JSON_ formatted version of the file's first two blocks including `txid`'s and addresses.

We might want to manipulate data more interactively, so there's 2 types of _callbacks_ that can be triggered:

* Block callbacks, a function called for every block processed
* Transaction callbacks, a function called for every transaction processed

You can trigger a callback by adding the `-ac` and/or `-bc` and/or `-tc` parameters, followed by the callback's key and an optional `-acparam` / `-bcparam` / `-tcparam` flag followed by a parameter.

These callbacks are respectively triggered:

- Once all blocks are loaded (`-ac` / `-acparam`)
- Once for each block loaded (`-bc` / `-bcparam`)
- Once for each transaction loaded (`-tc` / `-tcparam`)

As of now there are a couple of callbacks which are more demos than actual useful functions:

* `tdemo`, a demo of transaction processing, mainly printing addresses
* `countB`, simple processed blocks counter
* `countT`, simple processed transactions counter
* `csv`, outputs data in csv format, the wanted fields are passed as a parameter, for example:
```sh
$ ./goxplorer -t -b 1845 -a -n 1 -tc csv -tcparam outputs.address,txid|head -1
"1KFHE7w8BhaENAswwryaoccDb6qcT6DbYY,null,null,null", df882c43a0441b2f9b14ffd39738a6a36cda3b8c11279c7d0f93ccd5002c9482
```
* `mkdballaddr`, creates a _LevelDB_ database of **all** addresses/transactions, will result on an insanely huge database, don't use it.

And last but not least, `bruteforce`, which takes a `-bcparam` as a parameter and will try to bruteforce addresses against dictionaries:
```sh
$ ./goxplorer -a -b 1845 -bc bruteforce -bcparam wordlist/500-worst-passwords.txt
```

Here are some callbacks examples as defined in the `callbacks/callbacks.go` file:

```go
package callbacks

import (
	"encoding/json"
	"fmt"

	"gitlab.com/iMil/goxplorer/blockchain"
)

var Callbacks = map[string]interface{}{
	"tdemo":  TDemo,
	"countB": CountB,
	"countT": CountT,
	"csv":    PrintCSV,
}

func LsCallbacks() {
	for c := range Callbacks {
		fmt.Println(c)
	}
}

var count int = 1

// tDemo is a demo transaction callback that will print txid,
// in and out addresses
func TDemo(b blockchain.Block, t blockchain.TransacData) {
	if blockchain.Params.LoadTxid {
		sw := ""
		if t.SegWit {
			sw = "[segwit]"
		}
		fmt.Printf("txid: %v %s\n", t.Txid, sw)
	}
	for _, ins := range t.Ins {
		if len(ins.Address) > 0 { // coinbase has no inputs
			fmt.Printf("in addr: %v\n", ins.Address[0])
		}
	}
	for _, outs := range t.Outs {
		fmt.Printf("out addr: %v (%s)\n", outs.Address, outs.LockType)
		if outs.LockType == "P2PK" {
			k, _ := json.MarshalIndent(outs, "", "  ")
			fmt.Println(string(k))
		}
	}
}

// CountB is a demo block callback that will print the current
// processed blockchain
func CountB(b blockchain.Block) {
	fmt.Printf("New block processed %d\n", count)
	count++
}

// CountT is a demo transaction callback that will print the current
// processed blockchain
func CountT(b blockchain.Block, t blockchain.TransacData) {
	fmt.Printf("\r%d transactions", count)
	count++
}
```

Feel free to implement your owns!

# License

This software is distributed under the GNU General Public License, version 2

https://www.gnu.org/licenses/old-licenses/gpl-2.0.html

# Tip

Mandatory BTC address: `bc1q33m33lnp92le4h8aq2wwlp5xe2kjk3yg922jw7`

# Logo

The gopher logo was created with [Gopher Konstructor](https://github.com/quasilyte/gopherkon)

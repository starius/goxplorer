/*
 * Goxplorer, a blockchain file explorer
 * Copyright (C) 2019-2020 Emile `iMil" Heitor
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"gitlab.com/iMil/goxplorer/blockchain"
	"gitlab.com/iMil/goxplorer/callbacks"
	"gitlab.com/iMil/goxplorer/gendb"
)

var blkfilenum = flag.Uint64("b", 0, "block file number (i.e. 1845)")

func main() {
	a := flag.Bool("a", false, "load addresses (default: false)")
	addr := flag.String("addr", "",
		`address: search an address in the chainstate database (slow!)
mkdb: create an address database at $BTCADDRDB
db:<address> search address UTXOs at $BTCADDRDB
txid:<txid> search <txid> addresses, height and amounts`)
	d := flag.String("d", "",
		"load block file matching with a date of format 2019-12-24[@02:23:10]")
	e := flag.Int("e", 0,
		"start reading at block's height, autoloads blk file")
	blockfile := flag.String("f", "", "block file path")
	i := flag.Bool("i", false, "guess and load inputs addresses (default: false)")
	j := flag.Bool("j", false, "print loaded blocks in JSON (default: false)")
	l := flag.String("l", "",
		"start reading at block's hash, autoloads blk file")
	n := flag.Int("n", 0, "number of blocks to load (default: all)")
	r := flag.Bool("r", false, "load serialized raw ins and outs (default: false)")
	s := flag.Int("s", 0, "start at block #")
	t := flag.Bool("t", false, "compute and load TXIDs (default: false)")
	x := flag.Bool("x", false, "compute and load block hashes (default: false)")
	ac := flag.String("ac", "", "all blocks callback")
	acparam := flag.String("acparam", "", "all blocks callback optional parameter")
	bc := flag.String("bc", "", "block callback")
	bcparam := flag.String("bcparam", "",
		"block callback optional parameter")
	tc := flag.String("tc", "", "transaction callback")
	tcparam := flag.String("tcparam", "",
		"transaction callback optional parameter")
	tx := flag.String("tx", "", "raw hex serialized transaction")
	lscb := flag.Bool("lscb", false, "list callbacks")
	ldb := flag.String("ldb", "",
		"print LevelDB <key> infos for given parameter and exit")
	w := flag.Bool("w", false, "start HTTP REST server")

	flag.Parse()

	// set blocks and index home directories
	envCheck()

	var err error
	// start HTTP REST server
	if *w {
		err = os.Setenv("GOXHTTPMODE", "1")
		if err != nil {
			fatalErr(err)
		}
		startRESTSrv()
	}

	if flag.NFlag() < 1 {
		flag.Usage()
		os.Exit(1)
	}

	if *lscb {
		callbacks.LsCallbacks() // see callbacks/callbacks.go
		os.Exit(0)
	}

	bytype := bynone
	if len(*blockfile) > 0 { // block file path
		bytype = bypath
	}
	if *e > 0 { // block height as parameter
		bytype = byheight
	}
	if len(*l) > 0 { // block hash as parameter
		bytype = byhash
	}
	if len(*d) > 0 { // date as parameter
		bytype = bydate
	}
	if len(*tx) > 0 { // serialized transaction as parameter
		bytype = bytx
	}
	if bytype == bynone {
		bytype = byfile
	}

	// loading parameters, here we want to compute and load TXIDs, and load
	// inputs and outputs addresses. We also pass a helper function to the
	// transaction loading so we see something happening while reading the
	// block file.
	//
	// only load the n blocks from the block file
	blockchain.Params.NBlocks = uint32(*n)
	// compute and load TXIDs
	blockchain.Params.LoadTxid = *t
	// compute and load block hashes
	blockchain.Params.LoadBlockHash = *x
	// start loading at block s
	blockchain.Params.StartBlock = uint32(*s)
	// load output addresses
	blockchain.Params.LoadAddr = *a
	// guess and load input addresses
	blockchain.Params.LoadInAddr = *i
	// load in and out raw data
	blockchain.Params.LoadRaw = *r
	// execute the function mapped to this key for each block
	blockchain.Params.BlockFunc = callbacks.Callbacks[*bc]
	// parameters for the block callback
	blockchain.Params.BFuncParam = *bcparam
	// execute the function mapped to this key for each transaction
	blockchain.Params.TransacFunc = callbacks.Callbacks[*tc]
	// parameters for the transaction callback
	blockchain.Params.TFuncParam = *tcparam
	// execute the function mapped to this key for all blocks
	blockchain.Params.AllBlocksFunc = callbacks.Callbacks[*ac]
	// parameters for the all blocks callback
	blockchain.Params.ABFuncParam = *acparam

	var b []blockchain.Block

	// query leveldb and exit
	if len(*ldb) > 0 {
		var slb interface{}
		switch *ldb {
		case "block":
			slb, err = ReadLDB(gendb.LevelDB, *ldb, *l)
		case "file":
			slb, err = ReadLDB(gendb.LevelDB, *ldb, fmt.Sprintf("%d", *blkfilenum))
		}
		fatalErr(err)
		outJSON(slb)
		os.Exit(0)
	}
	// query chainstate to get height from address or txid
	var cs []blockchain.TChainState
	if len(*addr) > 0 {
		cs, err = blockchain.UtxoDecode(chainstate, *addr)
		if err != nil {
			fatalErr(err)
		}
		// chain query for address database creation
		if len(cs) == 0 && strings.HasPrefix(*addr, "mkdb:") {
			fmt.Printf("database '%s' created\n", addrdb)
			os.Exit(0)
		}
		if cs != nil {
			outJSON(cs)
		}
		os.Exit(0)
	}

	// -l or -e or -d were passed as parameters, retrieve block file name
	// and position through LevelDB
	if bytype == byhash || bytype == byheight || bytype == bydate {
		*blkfilenum, err = blkByType(bytype, *e, *l, *d)
		if err != nil {
			fatalErr(err)
		}
	}

	if bytype == bytx { // load a single serialized transaction
		err = mkBlock(&b, *tx)
	} else { // load a full block data file
		if bytype != bypath { // no file path was given, compute it
			*blockfile = fmt.Sprintf("%s/blk%05d.dat", blkhome, *blkfilenum)
		}
		b, err = blockchain.LoadBlocks(*blockfile)
		fatalErr(err)
	}
	// print loaded locks as JSON formatted data
	if *j {
		outJSON(b)
	}
}

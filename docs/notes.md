# Create a single block file

* Fetch block size

```sh
$ size=$(./goxplorer -b 1845 -n 1 -j|jq -r '.[0].size')
```

* Get size's block from regular block file

```sh
$ dd if=blocks/blk01845.dat of=1blk.dat count=1 bs=${size}
```


# Create a block index file

* Get the block hash

```sh
$ h=$(./goxplorer -b 1845 -j -n 1 -x|jq -r '.[0].blockhash')
```

* Convert it to Little Endian (62 is `b`):

```sh
rh=62$(echo ${h}|rev|dd conv=swab)
```

* Fetch datas from a complete block index database:

```sh
$ b=$(leveldbctl --dbdir=samples/testindex -xk g ${rh}|xxd -p -c 256|sed 's/0a$//')
```

* Write a mini block index database

```sh
$ leveldbctl --dbdir=minibi i # initialize the database
$ leveldbctl --dbdir=minibi -xk -xv p $rh $b
```

* Get a file record

```sh
x=$(printf "%04x" 1845)
f=66$(echo $x|rev|dd conv=swab)0000
r=$(leveldbctl --dbdir=samples/testindex -xk g ${f}|xxd -p|sed 's/0a$//')
```

66 == `f`
0000 == padding to have 4 bytes

* Write the file record

```sh
leveldbctl --dbdir=minibi -xk -xv p $f $r
```

# Create minimal chainstate database

* Fetch obfuscation key from real chainstate

```sh
$ leveldbctl --dbdir=chainstate -xk g 0e006f62667573636174655f6b6579|xxd -p
08458cbe37affcf01e0a
```

`0e006f62667573636174655f6b6579 == \x0e\x00obfuscate_key`

* Create mini db obfuscation key

Strip `0a` (newline)

```sh
$ leveldbctl --dbdir=minics -xk -xv p 0e006f62667573636174655f6b6579 08458cbe37affcf01e
```

* Insert an UTXO you like

```sh
$ ./goxplorer -addr db:addrdb:1M1LYV2jxTvhVnLV9ndXNrtZxXRUncDPK8
[
  {
    "txid": "62941e5444f09ce4bfe2d35f36c69ed7f8f077f07f6bbcac010227aed9b79655",
    "height": 601275,
    "nsize": 0,
    "address": "1M1LYV2jxTvhVnLV9ndXNrtZxXRUncDPK8",
    "amount": 1200
  }
]
$ txid=$(echo 62941e5444f09ce4bfe2d35f36c69ed7f8f077f07f6bbcac010227aed9b79655|rev|dd conv=swab)
$ leveldbctl --dbdir=chainstate -xk s 43${txid}|xxd -p -c 256|sed s/3a20/\ /; s/0a$//'
435596b7d9ae270201acbc6b7ff077f0f8d79ec6365fd3e2bfe49cf044541e946200 8d3dc85faf2782ca03bdde69ad6f67f01f65df73f55537fc940a
```

`\x3a\x20` == ": ", strip it

```sh
$ leveldbctl --dbdir=minics -xk -xv p 435596b7d9ae270201acbc6b7ff077f0f8d79ec6365fd3e2bfe49cf044541e946200 8d3dc85faf2782ca03bdde69ad6f67f01f65df73f55537fc940a
```

# Create minimal address database from here

```sh
$ BTCCHAINSTATE=minics ./goxplorer -addr mkdb:miniad
```


package gendb

import (
	"fmt"

	badger "github.com/dgraph-io/badger/v2"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/util"
)

type DBType uint8

type DB struct {
	Type DBType
	Conn interface{}
}

const (
	LevelDB  = DBType(0)
	BadgerDB = DBType(1)
)

// NewDB returns a *DB type with either a LevelDB or a Badger handler
func NewDB(t DBType, path string) (*DB, error) {
	var err error
	db := new(DB)
	db.Type = t
	switch t {
	case LevelDB:
		db.Conn, err = leveldb.OpenFile(path, nil)
	case BadgerDB:
		options := badger.DefaultOptions(path)
		options.Logger = nil
		db.Conn, err = badger.Open(options)
	}
	return db, err
}

// Close closes either a LevelDB or a Badger database connection
func (db *DB) Close() {
	switch db.Type {
	case LevelDB:
		ldb := db.Conn.(*leveldb.DB)
		ldb.Close()
	case BadgerDB:
		bdb := db.Conn.(*badger.DB)
		bdb.Close()
	}
}

// Get retrieves a value for `key` in either a LevelDB or a Badger database
func (db *DB) Get(key []byte) ([]byte, error) {
	var err error
	val := []byte{}
	switch db.Type {
	case LevelDB:
		var v []byte
		ldb := db.Conn.(*leveldb.DB)
		v, err = ldb.Get(key, nil)
		val = append(val, v...)
	case BadgerDB:
		bdb := db.Conn.(*badger.DB)
		err = bdb.View(func(txn *badger.Txn) error {
			item, err := txn.Get(key)
			if err != nil {
				return err
			}
			err = item.Value(func(v []byte) error {
				val = append(val, v...)
				return nil
			})
			return err
		})
	}
	return val, err
}

// Iterate iterates over a database, either LevelDB or Badger, with an optional
// prefix. and calls the function f with k and v as key and value retrieved
// from the iterated element. If f() returns true, then the iteration is
// stopped.
func (db *DB) Iterate(prefix []byte, f func(k, v []byte) (bool, error)) error {
	var err error
	brk := false
	switch db.Type {
	case LevelDB:
		ldb := db.Conn.(*leveldb.DB)
		iter := ldb.NewIterator(util.BytesPrefix(prefix), nil)
		for iter.Next() {
			brk, err = f(iter.Key(), iter.Value())
			if err != nil {
				return err
			}
			if brk {
				break
			}
		}
		iter.Release()
		return iter.Error()
	case BadgerDB:
		bdb := db.Conn.(*badger.DB)
		return bdb.View(func(txn *badger.Txn) error {
			iopt := badger.DefaultIteratorOptions
			iopt.Prefix = prefix
			iter := txn.NewIterator(iopt)
			defer iter.Close()
			for iter.Rewind(); iter.Valid(); iter.Next() {
				item := iter.Item()
				k := item.Key()
				err = item.Value(func(v []byte) error {
					brk, err = f(k, v)
					return err
				})
				if err != nil {
					return err
				}
				if brk {
					break
				}
			}
			return nil
		})
	}
	return fmt.Errorf("no such database type")
}

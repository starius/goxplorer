package main

import (
	"fmt"
	"os"
	"testing"

	"gitlab.com/iMil/goxplorer/blockchain"
)

const (
	wblock  = 1845
	wheight = 601629
	whash   = "000000000000000000147a7c7d848682df32f5c65e321f444ef7f18904f9a1f2"
	wdate   = "2019-10-30"
)

var testblkfile = fmt.Sprintf("%s/blk%05d.dat", os.Getenv("BTCBLOCKSHOME"), wblock)

func TestEnvCheck(t *testing.T) {
	pwd, _ := os.Getwd()
	os.Setenv("BTCBLOCKSHOME", pwd+"/samples")
	os.Setenv("BTCBLOCKINDEX", pwd+"/samples/testindex")

	_, err := os.Stat(os.Getenv("BTCBLOCKSHOME"))
	if err != nil {
		t.Error(err)
	}
	_, err = os.Stat(os.Getenv("BTCBLOCKINDEX"))
	if err != nil {
		t.Error(err)
	}
}

func TestBlkTypes(t *testing.T) {
	blktypes := []int{
		byheight,
		byhash,
		bydate,
	}

	for _, bt := range blktypes {
		blk, err := blkByType(bt, wheight, whash, wdate)
		if err != nil {
			t.Error(err)
		}
		if blk != wblock {
			t.Errorf("%d is not the expected blocks file number (%d)", blk, wblock)
		}
	}
}

func TestMkBlock(t *testing.T) {
	var b []blockchain.Block
	tx := "010000000001010000000000000000000000000000000000000000000000000000000000000000ffffffff64031d2e092cfabe6d6dfd4ad76684d950f0d552c65b65f4327f8cdc59dabf5667188a7a5af5945d29cf10000000f09f909f00104d696e6564206279207768787a383838000000000000000000000000000000000000000000000000000000050060290000000000000462c9044c000000001976a914c825a1ecf2a6830c4401620c3a16f1995057c2ab88ac00000000000000002f6a24aa21a9ed5234157ea6c81de0dbf0ced1969164328c4992e2f895deadbb1da2d75af1d23b08000000000000000000000000000000002c6a4c2952534b424c4f434b3a4c26568b3ec880eff3c406ace7c18d27cdd710de2c4e5f7c590fb22e001bf1730000000000000000266a24b9e11b6df3af861a7b874ec0565192fd6cc88e56fb1310d2ad37ca999da9112c0b8b72b101200000000000000000000000000000000000000000000000000000000000000000bd1c4c3a"
	txid := "df882c43a0441b2f9b14ffd39738a6a36cda3b8c11279c7d0f93ccd5002c9482"

	blockchain.Params.LoadTxid = true

	err := mkBlock(&b, tx)

	if err != nil {
		t.Error(err)
	}
	xlen := len(b)
	if xlen != 1 {
		t.Errorf("wrong block length %d", xlen)
	}
	xlen = len(b[0].Transac)
	if xlen != 1 {
		t.Errorf("wrong transaction length %d", xlen)
	}
	if b[0].Transac[0].Txid != txid {
		t.Errorf("wrong txid %s", b[0].Transac[0].Txid)
	}
}

package blockchain

import (
	"os"
	"path/filepath"
	"runtime"
	"testing"
)

var pkgdir = func() string {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		panic("No caller information")
	}
	return filepath.Dir(filename)
}()

var (
	samplesdir     = filepath.Join(pkgdir, "..", "samples")
	testchainstate = filepath.Join(samplesdir, "testchainstate")
	testaddrdb     = filepath.Join(samplesdir, "testaddrdb")
	testindex      = filepath.Join(samplesdir, "testindex")
)

func setTestEnv(t *testing.T) {
	if err := os.Setenv("BTCADDRDB", testaddrdb); err != nil {
		t.Fatal(err)
	}
	if err := os.Setenv("BTCBLOCKINDEX", testindex); err != nil {
		t.Fatal(err)
	}
	if err := os.Setenv("BTCBLOCKINDEX", testindex); err != nil {
		t.Fatal(err)
	}
}

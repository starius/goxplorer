package blockchain

import (
	"testing"
)

const wtxid = "62941e5444f09ce4bfe2d35f36c69ed7f8f077f07f6bbcac010227aed9b79655"
const waddr = "1M1LYV2jxTvhVnLV9ndXNrtZxXRUncDPK8"

func TestUTXO(t *testing.T) {
	uTests := []struct {
		label      string
		chainstate string
		address    string
	}{
		{"mkdb", testchainstate, "mkdb"},
		{"txid", testchainstate, "txid:62941e5444f09ce4bfe2d35f36c69ed7f8f077f07f6bbcac010227aed9b79655"},
		{"addr", "", "db:1M1LYV2jxTvhVnLV9ndXNrtZxXRUncDPK8"},
	}

	setTestEnv(t)

	for _, tt := range uTests {
		t.Run(tt.label, func(t *testing.T) {
			cs, err := UtxoDecode(tt.chainstate, tt.address)
			if err != nil {
				t.Fatal(err)
			}
			if tt.label == "mkdb" {
				return
			}
			if len(cs) < 1 {
				t.Fatal("empty result")
			}
			if tt.label == "txid" && cs[0].Address != waddr {
				t.Fatal("wrong address")
			}
			if tt.label == "addr" && cs[0].Txid != wtxid {
				t.Fatal("wrong txid")
			}
		})
	}
}

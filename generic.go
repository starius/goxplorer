// genetic.go contains generic functions for both CLI and REST usage

package main

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"gitlab.com/iMil/goxplorer/blockchain"
	"gitlab.com/iMil/goxplorer/gendb"
)

const (
	byfile   = 0
	byheight = 1
	byhash   = 2
	bydate   = 3
	bytx     = 4
	bypath   = 5
	bynone   = -1
)

var (
	blkhome    = os.Getenv("BTCBLOCKSHOME")
	blkindex   = os.Getenv("BTCBLOCKINDEX")
	chainstate = os.Getenv("BTCCHAINSTATE")
	addrdb     = os.Getenv("BTCADDRDB")
)

func fatalErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// envCheck checks that needed environment variables are set, and if not
// sets them with default values
func envCheck() {
	if blkhome == "" {
		blkhome = os.Getenv("HOME") + "/.bitcoin/blocks"
	}
	if blkindex == "" {
		blkindex = blkhome + "/index"
	}
	if chainstate == "" {
		chainstate = blkhome + "/chainstate"
	}
	if addrdb == "" {
		addrdb = "addrdb"
	}
}

// ReuseDB checks if a BadgerDB handle is not already available and returns
// it, with a second return value of true. Otherwise, it allocates a new
// handler for the current query with a second return value of false
func ReuseDB(t gendb.DBType) (*gendb.DB, bool, error) {
	var dbpath string
	if t == gendb.BadgerDB && blockchain.BDB != nil {
		db := new(gendb.DB)
		db.Type = gendb.BadgerDB
		db.Conn = blockchain.BDB
		return db, true, nil
	}
	switch t {
	case gendb.LevelDB:
		dbpath = blkindex
	case gendb.BadgerDB:
		dbpath = addrdb
	}
	db, err := gendb.NewDB(t, dbpath)
	if err != nil {
		return nil, false, err
	}

	return db, false, nil
}

// blkByType returns blockfile number according to either height, block hash,
// or a date, it is called by either command line or REST server
func blkByType(bytype int, e int, l string, d string) (uint64, error) {
	var data []byte
	var blk uint64
	var err error
	dbType := gendb.LevelDB

	if blockchain.BDB != nil {
		dbType = gendb.BadgerDB
	}
	db, reusedb, err := ReuseDB(dbType)
	if err != nil {
		return 0, err
	}
	if !reusedb {
		defer db.Close()
	}

	if bytype == byheight {
		data, err = blockchain.GetBlockRecordByHeight(db, uint64(e))
		if err != nil {
			return 0, err
		}
	}
	if bytype == byhash {
		data, err = blockchain.GetKeyVal(db, "b", l)
		if err != nil {
			return 0, err
		}
	}
	if bytype == bydate {
		blk, err = blockchain.GetFileRecordByDate(db, d)
		if err != nil {
			return 0, err
		}
	} else {
		// retrieve block file number and position from data given
		// by height or by hash
		blk, err = blockchain.ReadRecord(data, blockchain.BlockRecords, "nFile")
		if err != nil {
			return 0, err
		}
		dpos, err := blockchain.ReadRecord(data, blockchain.BlockRecords, "nDataPos")
		if err != nil {
			return 0, err
		}
		blockchain.Params.StartHash = dpos
		if err != nil {
			return 0, err
		}
	}
	return blk, nil
}

// mkBlock makes a blockchain.Block from a raw transaction string
func mkBlock(b *[]blockchain.Block, tx string) error {
	*b = make([]blockchain.Block, 1)
	// prepare a single block
	(*b)[0] = blockchain.Block{}
	// prepent hex transaction with the number of transactions
	raw, err := hex.DecodeString("01" + tx)
	if err != nil {
		return err
	}
	err = (*b)[0].ReadTransactions(raw)
	if err != nil {
		return err
	}
	return nil
}

// ReadLDB is a high level call to retrieve a value from LevelDB database
// using a `key` (`b`, `f`...) and a parameter (hash, blockfile, ...) as
// explained here https://en.bitcoin.it/wiki/Bitcoin_Core_0.11_(ch_2):_Data_Storage
func ReadLDB(t gendb.DBType, key string, param string) (interface{}, error) {
	db, reusedb, err := ReuseDB(t)
	if err != nil {
		return nil, err
	}
	if !reusedb {
		defer db.Close()
	}

	switch key {
	case "block":
		return blockchain.GetLDBBlock(db, param)
	case "file":
		return blockchain.GetLDBFile(db, param)
	}
	return nil, fmt.Errorf("no such key %s", key)
}

func outJSON(out interface{}) {
	js, err := json.MarshalIndent(out, "", "  ")
	fatalErr(err)
	fmt.Println(string(js))
}

package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/iMil/goxplorer/blockchain"
)

const wtxid = "62941e5444f09ce4bfe2d35f36c69ed7f8f077f07f6bbcac010227aed9b79655"
const waddr = "1M1LYV2jxTvhVnLV9ndXNrtZxXRUncDPK8"

func TestIndex(t *testing.T) {
	var recv map[string]string

	request, _ := http.NewRequest(http.MethodGet, "/", nil)
	response := httptest.NewRecorder()

	Index(response, request, nil)

	err := json.Unmarshal(response.Body.Bytes(), &recv)
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(recv, help) {
		t.Fatal("wrong message received")
	}
}

func utxoReply(t *testing.T, r []byte) []blockchain.TChainState {
	var uxreply []blockchain.TChainState
	err := json.Unmarshal(r, &uxreply)
	if err != nil || len(uxreply) < 1 {
		t.Fatal(err)
	}
	return uxreply
}

func TestBlock(t *testing.T) {
	prevtxid := "0000000000000000000000000000000000000000000000000000000000000000"
	tx := "010000000001010000000000000000000000000000000000000000000000000000000000000000ffffffff64031d2e092cfabe6d6dfd4ad76684d950f0d552c65b65f4327f8cdc59dabf5667188a7a5af5945d29cf10000000f09f909f00104d696e6564206279207768787a383838000000000000000000000000000000000000000000000000000000050060290000000000000462c9044c000000001976a914c825a1ecf2a6830c4401620c3a16f1995057c2ab88ac00000000000000002f6a24aa21a9ed5234157ea6c81de0dbf0ced1969164328c4992e2f895deadbb1da2d75af1d23b08000000000000000000000000000000002c6a4c2952534b424c4f434b3a4c26568b3ec880eff3c406ace7c18d27cdd710de2c4e5f7c590fb22e001bf1730000000000000000266a24b9e11b6df3af861a7b874ec0565192fd6cc88e56fb1310d2ad37ca999da9112c0b8b72b101200000000000000000000000000000000000000000000000000000000000000000bd1c4c3a"
	testRoutes := []struct {
		path    string
		url     string
		handler func(http.ResponseWriter, *http.Request, httprouter.Params)
	}{
		{"/block/:type/:parameter", "/block/file/01845", Blockfile},
		{"/block/:type/:parameter", "/block/height/601629", Blockfile},
		{"/block/:type/:parameter", "/block/hash/000000000000000000147a7c7d848682df32f5c65e321f444ef7f18904f9a1f2", Blockfile},
		{"/block/:type/:parameter", "/block/date/2019-10-30", Blockfile},
		{"/tx/", "/tx/", TxFile},
		{"/leveldb/:type/:parameter", "/leveldb/block/000000000000000000147a7c7d848682df32f5c65e321f444ef7f18904f9a1f2", LDBQuery},
		{"/leveldb/:type/:parameter", "/leveldb/file/1845", LDBQuery},
		{"/utxo/:type/:parameter", "/utxo/address/" + waddr, UTXOQuery},
		{"/utxo/:type/:parameter", "/utxo/txid/" + wtxid, UTXOQuery},
	}

	envCheck()

	// create address database
	_, err := blockchain.UtxoDecode(chainstate, "mkdb")
	if err != nil {
		t.Fatal(err)
	}

	for _, tr := range testRoutes {
		t.Run(fmt.Sprintf("route %s", tr.url), func(t *testing.T) {
			jsonStr := strings.NewReader(`{"nblocks": 1, "loadaddr": true, "tx": "` + tx + `"}`)

			router := httprouter.New()
			router.POST(tr.path, tr.handler)

			request, _ := http.NewRequest(http.MethodPost, tr.url, jsonStr)
			response := httptest.NewRecorder()

			router.ServeHTTP(response, request)

			if strings.HasPrefix(tr.url, "/utxo/address") {
				uxreply := utxoReply(t, response.Body.Bytes())
				if uxreply[0].Txid != wtxid {
					t.Fatal("wrong txid")
				}
			} else if strings.HasPrefix(tr.url, "/utxo/txid") {
				uxreply := utxoReply(t, response.Body.Bytes())
				if uxreply[0].Address != waddr {
					t.Fatal("wrong address")
				}
			} else if strings.HasPrefix(tr.url, "/leveldb/block") {
				var lbreply blockchain.TBlockRecords
				err := json.Unmarshal(response.Body.Bytes(), &lbreply)
				if err != nil {
					t.Fatal(err)
				}
				if lbreply.NHeight != 601629 {
					t.Fatal("wrong block height")
				}
			} else if strings.HasPrefix(tr.url, "/leveldb/file") {
				var lfreply blockchain.TFileRecords
				err := json.Unmarshal(response.Body.Bytes(), &lfreply)
				if err != nil {
					t.Fatal(err)
				}
				if lfreply.NHeightFirst != 600753 {
					t.Fatal("wrong first height")
				}
			} else {
				var blkreply []blockchain.Block
				err := json.Unmarshal(response.Body.Bytes(), &blkreply)
				if err != nil {
					t.Fatal(err)
				}
				if blkreply[0].Transac[0].Ins[0].PrevTxid != prevtxid {
					t.Fatal("wrong previous txid")
				}
			}
		})
	}
}

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/iMil/goxplorer/blockchain"
	"gitlab.com/iMil/goxplorer/gendb"
)

var (
	paramDefaults blockchain.SParams
	help          map[string]string
)

// jsonEncode encodes block data returned by LoadBlocks or mkBlock
func jsonEncode(w http.ResponseWriter, b interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(b)

	// restore default parameters
	blockchain.Params = paramDefaults
}

// retErr returns a JSON formatted error to HTTP stream
func retErr(w http.ResponseWriter, err error) {
	jsonEncode(w, map[string]string{"error": err.Error()})
}

// Index returns a basic usage for the HTTP server
func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	help = map[string]string{
		"/block/file/<file>":               "load this block file number",
		"/block/date/<date>":               "load block file matching with a date",
		"/block/height/<height>":           "start reading at block's height",
		"/block/hash/<hash>":               "start reading at block's hash",
		"/tx/":                             "process raw hex serialized transaction",
		"/leveldb/block/<hash>":            "print LevelDB block key values",
		"/leveldb/file/<blockfile number>": "print LevelDB file key values",
		"/utxo/<address>":                  "print UTXO data corresponding to address",
		"/utxo/<txid>":                     "print UTXO data corresponding to TXID",
	}
	jsonEncode(w, help)
}

// Blockfile is the handler for regular block file parsing requests
func Blockfile(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	var b []blockchain.Block
	var blk uint64
	var height int
	var blockfile string
	var err error

	json.NewDecoder(r.Body).Decode(&blockchain.Params)

	switch ps.ByName("type") {
	case "file":
		blockfile = fmt.Sprintf("%s/blk%s.dat", blkhome, ps.ByName("parameter"))
	case "height":
		height, err = strconv.Atoi(ps.ByName("parameter"))
		if err != nil {
			retErr(w, fmt.Errorf("conversion error"))
			return
		}
		blk, err = blkByType(byheight, height, "", "")
	case "hash":
		blk, err = blkByType(byhash, 0, ps.ByName("parameter"), "")
	case "date":
		blk, err = blkByType(bydate, 0, "", ps.ByName("parameter"))
	}
	if blockfile == "" {
		blockfile = fmt.Sprintf("%s/blk%05d.dat", blkhome, blk)
	}

	b, err = blockchain.LoadBlocks(blockfile)
	if err != nil {
		retErr(w, err)
		return
	}
	jsonEncode(w, b)
}

func LDBQuery(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	slb, err := ReadLDB(gendb.BadgerDB, ps.ByName("type"), ps.ByName("parameter"))
	if err != nil {
		retErr(w, err)
		return
	}
	jsonEncode(w, slb)
}

func UTXOQuery(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	var cs []blockchain.TChainState
	var err error
	switch ps.ByName("type") {
	case "address":
		cs, err = blockchain.UtxoDecode("", "db:"+ps.ByName("parameter"))
	case "txid":
		cs, err = blockchain.UtxoDecode(chainstate, "txid:"+ps.ByName("parameter"))
	}
	if err != nil {
		retErr(w, err)
		return
	}
	jsonEncode(w, cs)
}

func MkDBQuery(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	_, err := blockchain.UtxoDecode(chainstate, "mkdb")
	if err != nil {
		retErr(w, err)
		return
	}
	jsonEncode(w, map[string]string{"status": "done"})
}

// TxFile is the handler for raw transactions parsing requests
func TxFile(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var p struct {
		Tx string `json:"tx"`
	}
	var b []blockchain.Block

	// do not use json.Decode here as we have multiple sctructs to fill
	// with the same post data
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		retErr(w, err)
	}
	defer r.Body.Close()

	// retrieve raw transaction
	err = json.Unmarshal(body, &p)
	if err != nil {
		retErr(w, err)
	}
	err = json.Unmarshal(body, &blockchain.Params)
	if err != nil {
		retErr(w, err)
	}

	err = mkBlock(&b, p.Tx)
	if err != nil {
		retErr(w, err)
		return
	}

	jsonEncode(w, b)
}

// startRESTSrv starts the HTTP server and declares routes
func startRESTSrv() {
	router := httprouter.New()
	router.GET("/", Index)
	router.POST("/block/:type/:parameter", Blockfile)
	router.POST("/tx/", TxFile)
	router.GET("/leveldb/:type/:parameter", LDBQuery)
	router.GET("/utxo/:type/:parameter", UTXOQuery)
	router.GET("/mkdb/", MkDBQuery)

	paramDefaults = blockchain.Params

	log.Fatal(http.ListenAndServe(":7554", router))
}
